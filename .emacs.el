(require 'package)
; enable melpa repository
(add-to-list 'package-archives
             '("melpa" . "http://melpa.milkbox.net/packages/") t)
(package-initialize)

(load-theme 'ample t t)
(enable-theme 'ample)

(recentf-mode 1) ; show recent files in File menu

(global-linum-mode 1) ; display line numbers in margin. New in Emacs 23

; sml-mode
(setq auto-mode-alist
      (cons '("\.sml$" . sml-mode)
            (cons '("\.sig$" . sml-mode)
                  auto-mode-alist)))
(setq load-path (cons "~/.emacs.d/elpa/sml-mode-6.5" load-path))
(setq sml-program-name "/usr/lib/smlnj/bin/sml")(autoload 'sml-mode  "sml-mode" "Major mode for editing SML." t)

; integrate with system clipboard
(setq x-select-enable-clipboard t)
(setq interprogram-paste-function 'x-cut-buffer-or-selection-value)


;enable cua mode
(cua-mode t)





; enable evil-mode
(require 'evil)
(evil-mode 1)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
(put 'erase-buffer 'disabled nil)
